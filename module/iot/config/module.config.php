<?php
return array(
    'controllers' => array(
        'factories' => array(
            'iot\\V1\\Rpc\\RailwayNodes\\Controller' => 'iot\\V1\\Rpc\\RailwayNodes\\RailwayNodesControllerFactory',
            'iot\\V1\\Rpc\\Openweatcher\\Controller' => 'iot\\V1\\Rpc\\Openweatcher\\OpenweatcherControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'iot.rpc.railway-nodes' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/backend/railways/get/nodes',
                    'defaults' => array(
                        'controller' => 'iot\\V1\\Rpc\\RailwayNodes\\Controller',
                        'action' => 'railwayNodes',
                    ),
                ),
            ),
            'iot.rpc.openweatcher' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/backend/weatcher',
                    'defaults' => array(
                        'controller' => 'iot\\V1\\Rpc\\Openweatcher\\Controller',
                        'action' => 'openweatcher',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'iot.rpc.railway-nodes',
            1 => 'iot.rpc.openweatcher',
        ),
    ),
    'zf-rpc' => array(
        'iot\\V1\\Rpc\\RailwayNodes\\Controller' => array(
            'service_name' => 'RailwayNodes',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'iot.rpc.railway-nodes',
        ),
        'iot\\V1\\Rpc\\Openweatcher\\Controller' => array(
            'service_name' => 'openweatcher',
            'http_methods' => array(
                0 => 'POST',
            ),
            'route_name' => 'iot.rpc.openweatcher',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'iot\\V1\\Rpc\\RailwayNodes\\Controller' => 'Json',
            'iot\\V1\\Rpc\\Openweatcher\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'iot\\V1\\Rpc\\RailwayNodes\\Controller' => array(
                0 => 'application/vnd.iot.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'iot\\V1\\Rpc\\Openweatcher\\Controller' => array(
                0 => 'application/vnd.iot.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'iot\\V1\\Rpc\\RailwayNodes\\Controller' => array(
                0 => 'application/vnd.iot.v1+json',
                1 => 'application/json',
            ),
            'iot\\V1\\Rpc\\Openweatcher\\Controller' => array(
                0 => 'application/vnd.iot.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'iot\\V1\\Rpc\\RailwayNodes\\Controller' => array(
            'input_filter' => 'iot\\V1\\Rpc\\RailwayNodes\\Validator',
        ),
        'iot\\V1\\Rpc\\Openweatcher\\Controller' => array(
            'input_filter' => 'iot\\V1\\Rpc\\Openweatcher\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'iot\\V1\\Rpc\\RailwayNodes\\Validator' => array(),
        'iot\\V1\\Rpc\\Openweatcher\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'lat',
                'description' => 'lat',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'long',
                'description' => 'long',
            ),
        ),
    ),
);
