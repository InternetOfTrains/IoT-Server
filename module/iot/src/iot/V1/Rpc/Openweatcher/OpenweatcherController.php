<?php
namespace iot\V1\Rpc\Openweatcher;

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ApiProblem\ApiProblem;
use ZF\ApiProblem\ApiProblemResponse;
use ZF\ContentNegotiation\ViewModel;

class OpenweatcherController extends AbstractActionController
{
    public function openweatcherAction()
    {
        $array = json_decode($this->getRequest()->getContent());

        $lat = $array->lat;
        $long = $array->long;

        if($lat == null || $long == null) {
                return new ApiProblemResponse(new ApiProblem(404, "Not Found"));
        } else {
                $file = file_get_contents("http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&APPID=31bb608230d5aaf327ff73640a309496&lang=de");
                return new ViewModel(json_decode($file, true));
        }
    }
}
