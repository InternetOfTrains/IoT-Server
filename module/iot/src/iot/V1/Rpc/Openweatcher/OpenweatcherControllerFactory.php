<?php
namespace iot\V1\Rpc\Openweatcher;

class OpenweatcherControllerFactory
{
    public function __invoke($controllers)
    {
        return new OpenweatcherController();
    }
}
