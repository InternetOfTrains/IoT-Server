<?php
namespace iot\V1\Rpc\RailwayNodes;

class RailwayNodesControllerFactory
{
    public function __invoke($controllers)
    {
        return new RailwayNodesController();
    }
}
