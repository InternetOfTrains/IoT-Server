<?php
namespace iot\V1\Rpc\RailwayNodes;
ini_set('memory_limit', '-1');

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ContentNegotiation\ViewModel;

class RailwayNodesController extends AbstractActionController
{

    public function railwayNodesAction()
    {
        $json = $this->readfile_chunked('iot.getjson');
        $db = json_decode($json);

        $csv = $this->getCSV('stations.csv');

        $source = $db->features;

        foreach ($csv as $source_csv) {
            $init_array[$source_csv[4]] = $source_csv[2];
        }
        unset($init_array["Bf DS 100 Abk."]);


        foreach ($source as $hs) {
            $stcode = $hs->properties->railwayStationCode;

            if(isset($init_array[$stcode])) {
                $stations[] = array(
                    "name" => $hs->properties->geographicalName,
                    "geometry" => array(
                        "long" => $hs->geometry->coordinates[0],
                        "lat" => $hs->geometry->coordinates[1]
                    ),
                    "property" => array(
                        "StationCode" => $stcode,
                        "StationID" => $init_array[$stcode]
                    )
                );
            } else {
                $stations[] = array(
                    "name" => $hs->properties->geographicalName,
                    "geometry" => array(
                        "long" => $hs->geometry->coordinates[0],
                        "lat" => $hs->geometry->coordinates[1]
                    ),
                    "property" => array(
                        "StationCode" => $stcode,
                        "StationID" => "not_available"
                    )
                );
            }

        }

        return new ViewModel(
            array(
                "time" => time(),
                "status" => "200/OK",
                "Message" => "Query successful!",
                "stations" => $stations
            )
        );
    }


    function readfile_chunked($filename) {
        $handle = @fopen($filename, "r");
        if ($handle) {
            $content = "";
            while (($buffer = fgets($handle, 4096)) !== false) {
                $content .= $buffer;
            }
            fclose($handle);
        }

        return $content;
    }

    function getCSV($filename) {
        $file_handle = fopen($filename, 'r');
        while (!feof($file_handle)) {
            $line = fgets($file_handle);
            $csv_seperated_array[] = str_getcsv($line, ";");
        }

        fclose($file_handle);
        return $csv_seperated_array;
    }


    function getStationID($station_code, $station_id_array) {
        for($i = 1; $i < sizeof($station_id_array); $i++) {

            if($station_code == $station_id_array[$i]["StationCode"]) {
                return $station_id_array[$i]["StationID"];
            }
        }
    }

}