<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class ShowController extends AbstractActionController {


    public function indexAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->processPost();
        }


        $json = $this->readfile_chunked('data.json');
        $db = json_decode($json);

        $source = $db->data;


        $int = 0;
        $color_string = "{";
        $data = '[';
        foreach ($source as $hs) {
            $stcode = $hs->all_stability;

            if($stcode == 0) {
                $stations[] = array("NO_G$int", $stcode);
                $color_string .= "'NO_G$int':'#FF0000',";
                $data .= '[\'NO_G'.$int.'\',\'Hannover-Wolfsburg\',1,1],';
                $int++;
            } else {
                $stations[] = array("G$int", $stcode);
                $color_string .= "'G$int':'#00FF40',";
                $data .= '[\'G'.$int.'\',\'Hannover-Wolfsburg\',1,1],';
                $int++;
            }
        }
        $color_string = substr($color_string, 0, -1);
        $color_string .= "};";
        $data = substr($data, 0, -1);
        $data .= "];";


        return new ViewModel(array("data" => $data, "color" => $color_string));
    }


    public function processPost() {
        isset($this->getRequest()->getPost()->target) ? $target = $this->getRequest()->getPost()->target: $target = null;
        isset($this->getRequest()->getPost()->start) ? $target = $this->getRequest()->getPost()->start: $start = null;

    }

    function readfile_chunked($filename) {
        $handle = @fopen($filename, "r");
        if ($handle) {
            $content = "";
            while (($buffer = fgets($handle, 4096)) !== false) {
                $content .= $buffer;
            }
            fclose($handle);
        }

        return $content;
    }

}